const timer = ms => new Promise(res => setTimeout(res, ms));

const config = {
    rows: 10,
    cols: 10,
    initialFires: [[3, 3], [4, 5]],
    propagationProbability: 0.5
};

const stateClasses = {
    0: 'tree',
    1: 'fire',
    2: 'ashes'
};

function initGrid() {
    const grid = [];
    for (let rowNumber = 0; rowNumber < config.rows; rowNumber++) {
        const row = [];
        for (let colNumber = 0; colNumber < config.cols; colNumber++) {
            row.push(0);
        }
        grid.push(row);
    }
    config.initialFires.forEach(position => {
        const x = position[0];
        const y = position[1];
        grid[y][x] = 1;
    });
    return grid;
}

function drawGrid(grid) {
    const table = document.getElementById('grid');
    table.innerHTML = '';

    for (let rowNumber = 0; rowNumber < config.rows; rowNumber++) {
        const line = document.createElement('tr');
        for (let colNumber = 0; colNumber < config.cols; colNumber++) {
            const cell = document.createElement('td');
            cell.classList.add(stateClasses[grid[rowNumber][colNumber]]);
            line.appendChild(cell);
        }
        table.appendChild(line);
    }
}

function setAshes(grid, rowNumber, colNumber) {
    grid[rowNumber][colNumber] = 2;
}

function isFire(grid, rowNumber, colNumber) {
    return grid[rowNumber][colNumber] === 1;
}

function setFire(nextGrid, rowNumber, colNumber) {
    nextGrid[rowNumber][colNumber] = 1;
}

function isTree(grid, rowNumber, colNumber) {
    return grid[rowNumber][colNumber] === 0;
}

function isFireSpreading() {
    return Math.random() <= config.propagationProbability;
}

function simulateFireSpread(grid) {
    const nextGrid = structuredClone(grid);
    for (let rowNumber = 0; rowNumber < config.rows; rowNumber++) {
        for (let colNumber = 0; colNumber < config.cols; colNumber++) {
            if (!isFire(grid, rowNumber, colNumber)) continue;
            setAshes(nextGrid, rowNumber, colNumber);
            if (rowNumber > 0 && isTree(grid, rowNumber - 1, colNumber) && isFireSpreading()) {
                setFire(nextGrid, rowNumber - 1, colNumber);
            }
            if (rowNumber < config.rows - 1 && isTree(grid, rowNumber + 1, colNumber) && isFireSpreading()) {
                setFire(nextGrid, rowNumber + 1, colNumber);
            }
            if (colNumber > 0 && isTree(grid, rowNumber, colNumber - 1) && isFireSpreading()) {
                setFire(nextGrid, rowNumber, colNumber - 1);
            }
            if (colNumber < config.cols - 1 && isTree(grid, rowNumber, colNumber + 1) && isFireSpreading()) {
                setFire(nextGrid, rowNumber, colNumber + 1);
            }
        }
    }
    return nextGrid;
}

function isOnFire(grid) {
    return grid.some(row => row.some(cell => cell === 1));
}

async function run() {
    let grid = initGrid();
    drawGrid(grid);
    while (isOnFire(grid)) {
        await timer(1000);
        grid = simulateFireSpread(grid);
        drawGrid(grid);
    }
}

window.onload = () => run();
